package org.xmx0632.management.repository.mybatis;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.security.utils.Digests;
import org.springside.modules.test.spring.SpringTransactionalTestCase;
import org.springside.modules.utils.Encodes;
import org.xmx0632.management.data.UserData;
import org.xmx0632.management.entity.User;

import com.google.common.collect.Maps;

@DirtiesContext
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class UserMybatisDaoTest extends SpringTransactionalTestCase {

	@Autowired
	private UserMybatisDao userDao;

	@Test
	public void getUser() throws Exception {
		User user = userDao.get(1L);
		assertNotNull("User not found", user);
		assertEquals("admin", user.getLoginName());
	}

	@Test
	public void searchUser() throws Exception {
		Map<String, Object> parameter = Maps.newHashMap();
		parameter.put("name", "Admin");
		List<User> result = userDao.search(parameter);
		assertEquals(1, result.size());
		assertEquals("admin", result.get(0).getLoginName());
	}

	@Test
	public void createAndDeleteUser() throws Exception {
		// create
		int count = countRowsInTable("df_user");
		User user = UserData.randomNewUser();

		int HASH_INTERATIONS = 1024;
		int SALT_SIZE = 8;

		byte[] salt = Digests.generateSalt(SALT_SIZE);
		user.setSalt(Encodes.encodeHex(salt));

		byte[] hashPassword = Digests.sha1(user.getPlainPassword().getBytes(), salt, HASH_INTERATIONS);
		user.setPassword(Encodes.encodeHex(hashPassword));
		user.setRoles("user");

		userDao.save(user);
		Long id = user.getId();

		assertEquals(count + 1, countRowsInTable("df_user"));
		User result = userDao.get(id);
		assertEquals(user.getLoginName(), result.getLoginName());

		// delete
		userDao.delete(id);
		assertEquals(count, countRowsInTable("df_user"));
		assertNull(userDao.get(id));
	}

}
