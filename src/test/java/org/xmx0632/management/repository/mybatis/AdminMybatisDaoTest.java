package org.xmx0632.management.repository.mybatis;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.springside.modules.test.spring.SpringTransactionalTestCase;
import org.xmx0632.management.data.AdminData;
import org.xmx0632.management.entity.Admin;

import com.google.common.collect.Maps;

@DirtiesContext
@ContextConfiguration(locations = { "/applicationContext.xml" })
public class AdminMybatisDaoTest extends SpringTransactionalTestCase {

	@Autowired
	private AdminMybatisDao adminDao;

	@Test
	public void getUser() throws Exception {
		Admin user = adminDao.get(1L);
		assertNotNull("Admin not found", user);
		assertEquals("admin", user.getLoginName());
	}

	@Test
	public void searchUser() throws Exception {
		Map<String, Object> parameter = Maps.newHashMap();
		parameter.put("name", "管理员");
		List<Admin> result = adminDao.search(parameter);
		assertEquals(1, result.size());
		assertEquals("admin", result.get(0).getLoginName());
	}

	@Test
	public void createAndDeleteUser() throws Exception {
		// create
		String tableName = "df_admin";
		int count = countRowsInTable(tableName);
		Admin user = AdminData.randomNewUser();
		adminDao.save(user);
		Long id = user.getId();

		assertEquals(count + 1, countRowsInTable(tableName));
		Admin result = adminDao.get(id);
		assertEquals(user.getLoginName(), result.getLoginName());

		// delete
		adminDao.delete(id);
		assertEquals(count, countRowsInTable(tableName));
		assertNull(adminDao.get(id));
	}

}
