package org.xmx0632.management.data;

import org.springside.modules.test.data.RandomData;
import org.xmx0632.management.entity.Admin;

public class AdminData {

	public static Admin randomNewUser() {
		Admin admin = new Admin();
		admin.setLoginName(RandomData.randomName("admin"));
		admin.setName(RandomData.randomName("Admin"));
		admin.setPlainPassword(RandomData.randomName("password"));
		return admin;
	}
}
