package org.xmx0632.management.repository.mybatis;

import java.util.List;
import java.util.Map;

import org.xmx0632.management.entity.Admin;

/**
 * 通过@MapperScannerConfigurer扫描目录中的所有接口, 动态在Spring Context中生成实现.
 * 方法名称必须与Mapper.xml中保持一致.
 * 
 * @author calvin
 */
@MyBatisRepository
public interface AdminMybatisDao {

	Admin get(Long id);

	List<Admin> search(Map<String, Object> parameters);

	void save(Admin user);

	void delete(Long id);
}
