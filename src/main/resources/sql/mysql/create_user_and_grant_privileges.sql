use mysql;
DELETE FROM mysql.user WHERE User="management";
flush privileges;

--便于远程连接数据库
--insert into mysql.user(Host,User,Password,ssl_cipher,x509_issuer,x509_subject) values("localhost","management",password("management"),'','','');
insert into mysql.user(Host,User,Password,ssl_cipher,x509_issuer,x509_subject) values("%","management",password("management"),'','','');

--CREATE USER management IDENTIFIED BY 'management';
--grant ALL PRIVILEGES ON management.* TO 'management'@'%' IDENTIFIED BY 'management' WITH GRANT OPTION;
flush privileges;
commit;